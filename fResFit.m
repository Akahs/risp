function f_res = fResFit(x, eps_m, handles)
    f_res = zeros(size(x));
    for i = 1:length(x)
        [L1, L2] = fGeoFactor(x(i));
        x0_str = get(handles.edit_x0,'String');
        x0 = str2double(x0_str);
        myfun = @(f) fEps_e(f,handles)+ eps_m *(1/L1-1);
        f_res(i) = fzero(myfun,x0);
    end
end
