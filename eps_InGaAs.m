% Author: Kaijun Feng
% Last edited: 6/1/2015
% Note: permittivity for InGaAs. 
% Ref: [1] Anthony Hoffman's PhD dissertation.
% [2] http://www.ioffe.ru/SVA/NSM/Semicond/GaInAs/optic.html 
% [3] Solid State Comm. 136, 404 (2005)
% [4] Semicond. Sci. Technol. 2, 329 (1987)
function [re, im]=eps_InGaAs(omega, n)
% All frequencies in 1/cm
a = 1.237e4; % from curve fitting
c = 3e10; % cm/s
omega_LO_InAs = 233; % InAs-like mode
omega_TO_InAs = 225;
gamma_InAs = 4;
omega_LO_GaAs = 269;
omega_TO_GaAs = 255;
gamma_GaAs = 2.4;
omega_p = a*sqrt(n)/c; % plasma frequency
eps_inf = 11.6;
gamma = 1.5e12/c; % plasma damping
% eps_intrin = eps_inf*...
%     (omega^2-omega_LO_InAs^2+1i*omega*gamma_LO_InAs)*...
%     (omega^2-omega_LO_GaAs^2+1i*omega*gamma_LO_GaAs)*...
%     1/(omega^2-omega_TO_InAs^2+1i*omega*gamma_TO_InAs)*...
%     1/(omega^2-omega_TO_GaAs^2+1i*omega*gamma_TO_GaAs);
eps_plasma = omega_p^2/(omega^2+1i*omega*gamma);
f1 = (omega_LO_GaAs^2-omega_TO_InAs^2)/(omega_TO_GaAs^2-omega_TO_InAs^2); % oscillator strength
eps_phonon1 = f1*(omega_LO_InAs^2-omega_TO_InAs^2)/(omega_TO_InAs^2-omega^2-1i*omega*gamma_InAs);
f2 = (omega_TO_GaAs^2-omega_LO_InAs^2)/(omega_TO_GaAs^2-omega_TO_InAs^2);
eps_phonon2 = f2*(omega_LO_GaAs^2-omega_TO_GaAs^2)/(omega_TO_GaAs^2-omega^2-1i*omega*gamma_GaAs);
eps = eps_inf*(1+eps_phonon1+eps_phonon2-eps_plasma);
re = real(eps);
im = imag(eps);
end