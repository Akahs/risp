function fRes(handles)
    global xmin xmax xdata ydata ar f_res
    ar=linspace(xmin,xmax,200);
    x0 = 1100;
    f_res = zeros(1,200);
    eps_str = get(handles.edit_eps,'String');
    eps_m = str2double(eps_str);
    for i = 1:200
        [L1, L2] = fGeoFactor(ar(i));
        myfun = @(f)fEps_e(f,handles)+eps_m*(1/L1-1);
        f_res(i) = fzero(myfun,x0);
    end
    plot(handles.axes1,ar,f_res);
    hold on;
    plot(xdata,ydata,'x');
    hold off;
    xlim([xmin xmax]);
    xlabel('Aspect ratio');
    ylabel('Resonance frequency (1/cm)');
end