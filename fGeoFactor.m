function [L1,L2] = fGeoFactor(ar)
    % ar is the aspect ratio w/h, L1 is always the longitudinal geometry
    % factor
    if ar == 1
        L1 = 1/3;
        L2 = 1/3;
        return
    elseif ar < 1 % prolate
        e = sqrt(1-ar^2);
        L1 = (1-e^2)/e^2*(-1+(1/(2*e))*log((1+e)/(1-e)));
        L2 = (1-L1)/2;         
    else % oblate
        e = sqrt(1 - 1/ar^2);
        g = sqrt((1-e^2)/e^2);
        L2 = g/(2*e^2)*(pi/2-atan(g))-g^2/2;   
        L1 = 1 - 2*L2;
    end
end
