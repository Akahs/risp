ar=linspace(0.3,1.2,200);
x0 = 1100;
f_res = zeros(1,200);
for i = 1:200
    [L1, L2] = fGeoFactor(ar(i));
    myfun = @(f)fEps_e(f)+1/L1-1;
    f_res(i) = fzero(myfun,x0);
end
plot(ar,f_res);