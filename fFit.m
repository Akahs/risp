function fFit(handles)
    global xdata ydata xmin xmax ar f_res
    target = @(eps_m,x) fResFit(x,eps_m,handles);
%     target = fittype('fResFit(x, eps_m)');
    [result,gof]= fit(xdata,ydata,target,'StartPoint',1,'Exclude',xdata<xmin|xdata>xmax);
    eps_m=coeffvalues(result);
    set(handles.edit_eps,'String',num2str(eps_m,4));
    ar = linspace(xmin,xmax);
    f_res = fResFit(ar,eps_m,handles);
    plot(ar,f_res);
    hold on;
    plot(xdata,ydata,'x');
    xlim([xmin xmax]);
    hold off;
end
   